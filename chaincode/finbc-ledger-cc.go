package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"math/rand"
)

const (
	CHAINCODE_SCM_VERSION    = "[COMMIT_ID]"
	CHAINCODE_CONTRACT_ASSET = "chaincode.contract.asset"
	CHAINCODE_CONFIG         = "chaincode.config"
)

type FinBcContract struct {
}

type FinBcContractAsset struct {
	DocType string `json:"docType"`
	Count   int64  `json:"count"`
}

type GetInformationRequest struct {
	IncreaseCount bool `json:"increaseCount"`
}

type GetInformationResponse struct {
	DocType             string `json:"docType"`
	ChainCodeScmVersion string `json:"chainCodeScmVersion"`
	AccessCount         int64  `json:"count"`
}

type Account struct {
	DocType    string                 `json:"docType"`
	Id         string                 `json:"id"`
	ExternalId string                 `json:"externalId"`
	Attributes map[string]interface{} `json:"attributes"`
	PubKey     string                 `json:"pubKey"`
	EncKey     string                 `json:"encKey"`
	EncData    string                 `json:"encData"`
	Token      string                 `json:"token"`
}

type CoinWallet struct {
	DocType       string `json:"docType"`
	Id            string `json:"id"`
	Owner         string `json:"owner"`
	EncOwnerValue string `json:"encOwnerValue"`
	EncFinBcValue string `json:"encFinBcValue"`
	Token         string `json:"token"`
}

type Config struct {
	DocType      string `json:"docType"`
	Id           string `json:"id"`
	FinBcAccount string `json:"finBcAccount"`
	FinBcPubKey  string `json:"finBcPubKey"`
	Token        string `json:"token"`
}

type InvestmentApprovalBook struct {
	DocType string `json:"docType"`
	Id      string `json:"id"`
	Owner   string `json:"owner"`
	EncData string `json:"encData"`
	Token   string `json:"token"`
}

type InvoiceStore struct {
	DocType string `json:"docType"`
	Id      string `json:"id"`
	Owner   string `json:"owner"`
	EncData string `json:"encData"`
	Token   string `json:"token"`
}

type InvestmentApproval struct {
	DocType string `json:"docType"`
	Id      string `json:"id"`
	EncData string `json:"encData"`
	Token   string `json:"token"`
	State   string `json:"state"`
}

type Invoice struct {
	DocType string `json:"docType"`
	Id      string `json:"id"`
	EncData string `json:"encData"`
	Token   string `json:"token"`
}

type ProcessOrder struct {
	DocType   string `json:"docType"`
	Id        string `json:"id"`
	Created   string `json:"created"`
	Updated   string `json:"updated"`
	Token     string `json:"token"`
	Recipient string `json:"recipient"`
	Sender    string `json:"sender"`
	EncData   string `json:"encData"`
	EncKey    string `json:"encKey"`
	State     string `json:"state"`
}

type ConfigRequest struct {
	Config Config `json:"config"`
}

type CoinDepositRequest struct {
	Id            string `json:"id"`
	Token         string `json:"token"`
	ExternalId    string `json:"externalId"`
	InternalTxId  string `json:"internalTxId"`
	EncOwnerValue string `json:"encOwnerValue"`
	EncFinBcValue string `json:"encFinBcValue"`
	Signature     string `json:"signature"`
}

type FactorInvoiceRequest struct {
	Invoice          Invoice           `json:"invoice"`
	Wallets          []CoinWallet      `json:"wallets"`
	ProcessOrder     ProcessOrder      `json:"invoiceRef"`
	ProcessOrderRefs []ProcessOrderRef `json:"processOrderRefs"`
}

type FactorInvoiceResponse struct {
	Invoice    Invoice      `json:"invoice"`
	InvoiceRef ProcessOrder `json:"invoiceRef"`
}

type UpdateWalletsRequest struct {
	Wallets []CoinWallet `json:"wallets"`
}

type ProcessOrderRef struct {
	Id    string `json:"id"`
	Token string `json:"token"`
}

type ProcessOrdersRequest struct {
	ProcessOrders []ProcessOrder `json:"processOrders"`
}

type ProcessInvoiceTokenTransfersRequest struct {
	InvoiceStore     InvoiceStore      `json:"invoiceStore"`
	ProcessOrderRefs []ProcessOrderRef `json:"processOrderRefs"`
}

type ProcessInvoicePaymentRequest struct {
	Invoice          Invoice           `json:"invoice"`
	Wallets          []CoinWallet      `json:"wallets"`
	ProcessOrderRefs []ProcessOrderRef `json:"processOrderRefs"`
}

type ProcessInvestmentApprovalTokenTransfersRequest struct {
	InvestmentApprovalBook InvestmentApprovalBook `json:"investmentApprovalBook"`
	ProcessOrderRefs       []ProcessOrderRef      `json:"processOrderRefs"`
}

type CreateInvoiceRequest struct {
	InvoiceStore InvoiceStore `json:"invoiceStore"`
	Invoice      Invoice      `json:"invoice"`
}

type CreateInvoiceResponse struct {
	InvoiceStore InvoiceStore `json:"invoiceStore"`
	Invoice      Invoice      `json:"invoice"`
}

type CreateInvestmentApprovalRequest struct {
	InvestmentApprovalBook InvestmentApprovalBook `json:"investmentApprovalBook"`
	InvestmentApproval     InvestmentApproval     `json:"investmentApproval"`
}

type CreateInvestmentApprovalResponse struct {
	InvestmentApprovalBook InvestmentApprovalBook `json:"investmentApprovalBook"`
	InvestmentApproval     InvestmentApproval     `json:"investmentApproval"`
}

type CreateAccountRequest struct {
	Account                Account                `json:"account"`
	Wallet                 CoinWallet             `json:"wallet"`
	InvoiceStore           InvoiceStore           `json:"invoiceStore"`
	InvestmentApprovalBook InvestmentApprovalBook `json:"investmentApprovalBook"`
}

type CreateAccountResponse struct {
	Account                Account                `json:"account"`
	Wallet                 CoinWallet             `json:"coinWallet"`
	InvoiceStore           InvoiceStore           `json:"invoiceStore"`
	InvestmentApprovalBook InvestmentApprovalBook `json:"investmentApprovalBook"`
}

type UpdateAccountRequest struct {
	Account                Account                `json:"account"`
	Wallet                 CoinWallet             `json:"coinWallet"`
	InvoiceStore           InvoiceStore           `json:"invoiceStore"`
	InvestmentApprovalBook InvestmentApprovalBook `json:"investmentApprovalBook"`
}

type UpdateAccountResponse struct {
	Account                Account                `json:"account"`
	Wallet                 CoinWallet             `json:"coinWallet"`
	InvoiceStore           InvoiceStore           `json:"invoiceStore"`
	InvestmentApprovalBook InvestmentApprovalBook `json:"investmentApprovalBook"`
}

type UpdateInvoiceRequest struct {
	Invoice Invoice `json:"invoice"`
}

type UpdateInvoiceResponse struct {
	Invoice Invoice `json:"invoice"`
}

type UpdateInvestmentApprovalRequest struct {
	InvestmentApproval InvestmentApproval `json:"investmentApproval"`
}

type UpdateInvestmentApprovalResponse struct {
	InvestmentApproval InvestmentApproval `json:"investmentApproval"`
}

func (fc *FinBcContract) Init(stub shim.ChaincodeStubInterface) peer.Response {

	err := initChaincodeContractAsset(stub)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func initChaincodeContractAsset(stub shim.ChaincodeStubInterface) error {

	asByte, err := read(stub, CHAINCODE_CONTRACT_ASSET)
	if err != nil {
		infoAsset := FinBcContractAsset{}
		infoAsset.DocType = "InfoAsset"
		infoAsset.Count = 0

		asByte, err = json.Marshal(infoAsset)
		if err != nil {
			return err
		}

		err = stub.PutState(CHAINCODE_CONTRACT_ASSET, asByte)
		if err != nil {
			return err
		}
	} else {
		fmt.Println(CHAINCODE_CONTRACT_ASSET + " exist.")
	}

	asByte, err = read(stub, CHAINCODE_CONFIG)
	if err != nil {
		config := Config{}
		config.DocType = "Config"
		config.Id = CHAINCODE_CONFIG
		config.FinBcAccount = "notsetyet"
		config.Token = nextToken()

		asByte, err = json.Marshal(config)
		if err != nil {
			return err
		}

		err = stub.PutState(CHAINCODE_CONFIG, asByte)
		if err != nil {
			return err
		}
	} else {
		fmt.Println(CHAINCODE_CONFIG + " exist.")
	}

	return nil
}

func (fc *FinBcContract) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()

	fmt.Println("Invoke is running " + function + " " + args[0])

	if function == "getInformation" {
		return fc.getInformation(stub, args)
	} else if function == "createAccount" {
		return fc.createAccount(stub, args)
	} else if function == "updateAccount" {
		return fc.updateAccount(stub, args)
	} else if function == "updateWallets" {
		return fc.updateWallets(stub, args)
	} else if function == "createInvoice" {
		return fc.createInvoice(stub, args)
	} else if function == "updateInvoice" {
		return fc.updateInvoice(stub, args)
	} else if function == "factorInvoice" {
		return fc.factorInvoice(stub, args)
	} else if function == "createInvestmentApproval" {
		return fc.createInvestmentApproval(stub, args)
	} else if function == "updateInvestmentApproval" {
		return fc.updateInvestmentApproval(stub, args)
	} else if function == "cancelInvestmentApproval" {
		return fc.updateInvestmentApproval(stub, args)
	} else if function == "addProcessOrders" {
		return fc.addProcessOrders(stub, args)
	} else if function == "processInvoiceTokenTransfers" {
		return fc.processInvoiceTokenTransfers(stub, args)
	} else if function == "processInvoicePayment" {
		return fc.processInvoicePayment(stub, args)
	} else if function == "processInvestmentApprovalTokenTransfers" {
		return fc.processInvestmentApprovalTokenTransfers(stub, args)
	} else if function == "updateConfig" {
		return fc.updateConfig(stub, args)
	} else if function == "query" {
		return fc.query(stub, args)
	}

	return shim.Error("Invalid Smart Contract function name: " + function)
}

func (fc *FinBcContract) getInformation(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	asByte, err := read(stub, CHAINCODE_CONTRACT_ASSET)
	if err != nil {
		return shim.Error(err.Error())
	}

	infoAsset := FinBcContractAsset{}
	err = json.Unmarshal(asByte, &infoAsset)
	if err != nil {
		return shim.Error(err.Error())
	}

	getInformationRequest := GetInformationRequest{}
	asByte = []byte(args[0])
	err = json.Unmarshal(asByte, &getInformationRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	if getInformationRequest.IncreaseCount {
		infoAsset.Count = infoAsset.Count + 1

		asByte, err = json.Marshal(infoAsset)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(CHAINCODE_CONTRACT_ASSET, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}

		// it's only for bugfixing
		//config := Config{}
		//config.DocType = "Config"
		//config.Id = CHAINCODE_CONFIG
		//config.FinBcAccount = "notsetyet"
		//config.Token = nextToken()

		//asByte, err = json.Marshal(config)
		//if err != nil {
		//	return shim.Error(err.Error())
		//}

		//err = stub.PutState(CHAINCODE_CONFIG, asByte)
		//if err != nil {
		//	return shim.Error(err.Error())
		//}
	}

	infoRet := GetInformationResponse{}
	infoRet.DocType = infoAsset.DocType
	infoRet.ChainCodeScmVersion = CHAINCODE_SCM_VERSION
	infoRet.AccessCount = infoAsset.Count

	asByte, err = json.Marshal(infoRet)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(asByte)
}

func (fc *FinBcContract) createAccount(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	createAccountRequest := CreateAccountRequest{}
	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &createAccountRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	account := createAccountRequest.Account
	account.DocType = "Account"
	account.Token = nextToken()

	coinWallet := createAccountRequest.Wallet
	coinWallet.DocType = "CoinWallet"
	coinWallet.Id = nextToken()
	coinWallet.Token = nextToken()

	invoiceStore := createAccountRequest.InvoiceStore
	invoiceStore.DocType = "InvoiceStore"
	invoiceStore.Id = nextToken()
	invoiceStore.Token = nextToken()

	investmentApprovalBook := createAccountRequest.InvestmentApprovalBook
	investmentApprovalBook.DocType = "InvestmentApprovalBook"
	investmentApprovalBook.Id = nextToken()
	investmentApprovalBook.Token = nextToken()

	asByte, err = json.Marshal(coinWallet)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(coinWallet.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = json.Marshal(invoiceStore)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(invoiceStore.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = json.Marshal(investmentApprovalBook)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(investmentApprovalBook.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = json.Marshal(account)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(account.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	createAccountResponse := CreateAccountResponse{}
	createAccountResponse.Account = account
	createAccountResponse.Wallet = coinWallet
	createAccountResponse.InvoiceStore = invoiceStore
	createAccountResponse.InvestmentApprovalBook = investmentApprovalBook

	asByte, err = json.Marshal(createAccountResponse)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(asByte)

}

func (fc *FinBcContract) createInvoice(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	createInvoiceRequest := CreateInvoiceRequest{}
	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &createInvoiceRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = read(stub, createInvoiceRequest.InvoiceStore.Id)
	if err != nil {
		return shim.Error(err.Error())
	}

	orgInvoiceStore := InvoiceStore{}
	err = json.Unmarshal(asByte, &orgInvoiceStore)
	if err != nil {
		return shim.Error(err.Error())
	}

	if orgInvoiceStore.Token != createInvoiceRequest.InvoiceStore.Token {
		return shim.Error("Wrong invoicestore token!")
	}

	invoiceStore := createInvoiceRequest.InvoiceStore
	invoiceStore.DocType = "InvoiceStore"
	invoiceStore.Token = nextToken()

	asByte, err = json.Marshal(invoiceStore)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(invoiceStore.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	invoice := createInvoiceRequest.Invoice
	invoice.DocType = "Invoice"
	invoice.Token = nextToken()

	asByte, err = json.Marshal(invoice)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(invoice.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	createInvoiceResponse := CreateInvoiceResponse{}
	createInvoiceResponse.InvoiceStore = invoiceStore
	createInvoiceResponse.Invoice = invoice

	asByte, err = json.Marshal(createInvoiceResponse)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(asByte)

}

func (fc *FinBcContract) createInvestmentApproval(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	createInvestmentApprovalRequest := CreateInvestmentApprovalRequest{}
	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &createInvestmentApprovalRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = read(stub, createInvestmentApprovalRequest.InvestmentApprovalBook.Id)
	if err != nil {
		return shim.Error(err.Error())
	}

	orgInvestmentApprovalBook := InvestmentApprovalBook{}
	err = json.Unmarshal(asByte, &orgInvestmentApprovalBook)
	if err != nil {
		return shim.Error(err.Error())
	}

	if orgInvestmentApprovalBook.Token != createInvestmentApprovalRequest.InvestmentApprovalBook.Token {
		return shim.Error("Wrong investmentapprovalbook token!")
	}

	investmentApprovalBook := createInvestmentApprovalRequest.InvestmentApprovalBook
	investmentApprovalBook.DocType = "InvestmentApprovalBook"
	investmentApprovalBook.Token = nextToken()

	asByte, err = json.Marshal(investmentApprovalBook)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(investmentApprovalBook.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	investmentApproval := createInvestmentApprovalRequest.InvestmentApproval
	investmentApproval.DocType = "InvestmentApproval"
	investmentApproval.Token = nextToken()

	if investmentApproval.State != "Open" {
		return shim.Error("InvestmentApproval in wrong state, Open expected!")
	}

	asByte, err = json.Marshal(investmentApproval)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(investmentApproval.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	createInvestmentApprovalResponse := CreateInvestmentApprovalResponse{}
	createInvestmentApprovalResponse.InvestmentApprovalBook = investmentApprovalBook
	createInvestmentApprovalResponse.InvestmentApproval = investmentApproval

	asByte, err = json.Marshal(createInvestmentApprovalResponse)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(asByte)

}

func (fc *FinBcContract) updateInvoice(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	updateInvoiceRequest := UpdateInvoiceRequest{}
	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &updateInvoiceRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = read(stub, updateInvoiceRequest.Invoice.Id)
	if err != nil {
		return shim.Error(err.Error())
	}

	orgInvoice := Invoice{}
	err = json.Unmarshal(asByte, &orgInvoice)
	if err != nil {
		return shim.Error(err.Error())
	}

	if orgInvoice.Token != updateInvoiceRequest.Invoice.Token {
		return shim.Error("Wrong invoice token!")
	}

	invoice := updateInvoiceRequest.Invoice
	invoice.DocType = "Invoice"
	invoice.Token = nextToken()

	asByte, err = json.Marshal(invoice)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(invoice.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	updateInvoiceResponse := UpdateInvoiceResponse{}
	updateInvoiceResponse.Invoice = invoice

	asByte, err = json.Marshal(updateInvoiceResponse)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(asByte)

}

func (fc *FinBcContract) factorInvoice(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	factorInvoiceRequest := FactorInvoiceRequest{}
	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &factorInvoiceRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = read(stub, factorInvoiceRequest.Invoice.Id)
	if err != nil {
		return shim.Error(err.Error())
	}

	orgInvoice := Invoice{}
	err = json.Unmarshal(asByte, &orgInvoice)
	if err != nil {
		return shim.Error(err.Error())
	}

	if orgInvoice.Token != factorInvoiceRequest.Invoice.Token {
		return shim.Error("Wrong invoice token!")
	}

	invoice := factorInvoiceRequest.Invoice
	invoice.DocType = "Invoice"
	invoice.Token = nextToken()

	asByte, err = json.Marshal(invoice)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(invoice.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	// update wallets
	for _, wallet := range factorInvoiceRequest.Wallets {

		asByte, err = read(stub, wallet.Id)
		if err != nil {
			return shim.Error(err.Error())
		}

		orgWallet := CoinWallet{}
		err = json.Unmarshal(asByte, &orgWallet)
		if err != nil {
			return shim.Error(err.Error())
		}

		if orgWallet.Token != wallet.Token {
			return shim.Error("Wrong wallet token!")
		}

		wallet.DocType = "CoinWallet"
		wallet.Token = nextToken()

		asByte, err = json.Marshal(wallet)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(wallet.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}

	}

	// persist transfer processorder
	processOrder := factorInvoiceRequest.ProcessOrder
	processOrder.DocType = "ProcessOrder"
	processOrder.Token = nextToken()

	asByte, err = json.Marshal(processOrder)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(processOrder.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	for _, processOrderRef := range factorInvoiceRequest.ProcessOrderRefs {

		asByte, err = read(stub, processOrderRef.Id)
		if err != nil {
			return shim.Error(err.Error())
		}

		processOrder := ProcessOrder{}
		err = json.Unmarshal(asByte, &processOrder)
		if err != nil {
			return shim.Error(err.Error())
		}

		if processOrder.Token != processOrderRef.Token {
			return shim.Error("Wrong processOrder token!")
		}

		if processOrder.State != "Open" {
			return shim.Error("ProcessOrder in wrong state!")
		}

		processOrder.State = "Done"
		processOrder.Token = nextToken()

		asByte, err = json.Marshal(processOrder)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(processOrder.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}
	}

	factorInvoiceResponse := FactorInvoiceResponse{}
	factorInvoiceResponse.Invoice = invoice
	factorInvoiceResponse.InvoiceRef = processOrder

	asByte, err = json.Marshal(factorInvoiceResponse)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(asByte)

}

func (fc *FinBcContract) updateInvestmentApproval(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	updateInvestmentApprovalRequest := UpdateInvestmentApprovalRequest{}
	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &updateInvestmentApprovalRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = read(stub, updateInvestmentApprovalRequest.InvestmentApproval.Id)
	if err != nil {
		return shim.Error(err.Error())
	}

	orgInvestmentApproval := InvestmentApproval{}
	err = json.Unmarshal(asByte, &orgInvestmentApproval)
	if err != nil {
		return shim.Error(err.Error())
	}

	if orgInvestmentApproval.Token != updateInvestmentApprovalRequest.InvestmentApproval.Token {
		return shim.Error("Wrong investmentApproval token!")
	}

	investmentApproval := updateInvestmentApprovalRequest.InvestmentApproval
	investmentApproval.DocType = "InvestmentApproval"
	investmentApproval.Token = nextToken()

	asByte, err = json.Marshal(investmentApproval)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(investmentApproval.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	updateInvestmentApprovalResponse := UpdateInvestmentApprovalResponse{}
	updateInvestmentApprovalResponse.InvestmentApproval = investmentApproval

	asByte, err = json.Marshal(updateInvestmentApprovalResponse)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(asByte)

}

func (fc *FinBcContract) cancelInvestmentApproval(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	updateInvestmentApprovalRequest := UpdateInvestmentApprovalRequest{}
	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &updateInvestmentApprovalRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = read(stub, updateInvestmentApprovalRequest.InvestmentApproval.Id)
	if err != nil {
		return shim.Error(err.Error())
	}

	if updateInvestmentApprovalRequest.InvestmentApproval.State != "Canceled" {
		return shim.Error("InvestmentApproval in wrong state, Canceled expected!")
	}

	return fc.updateInvestmentApproval(stub, args)

}

func (fc *FinBcContract) updateAccount(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	updateAccountRequest := UpdateAccountRequest{}
	updateAccountResponse := UpdateAccountResponse{}

	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &updateAccountRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = read(stub, updateAccountRequest.Account.Id)
	if err != nil {
		return shim.Error(err.Error())
	}

	orgAccount := Account{}
	err = json.Unmarshal(asByte, &orgAccount)
	if err != nil {
		return shim.Error(err.Error())
	}

	if orgAccount.Token != updateAccountRequest.Account.Token {
		return shim.Error("Wrong account token!")
	}

	account := updateAccountRequest.Account
	account.DocType = "Account"
	account.Token = nextToken()

	asByte, err = json.Marshal(account)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(account.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	updateAccountResponse.Account = account

	if (CoinWallet{}) != updateAccountRequest.Wallet {
		asByte, err = read(stub, updateAccountRequest.Wallet.Id)
		if err != nil {
			return shim.Error(err.Error())
		}

		orgWallet := CoinWallet{}
		err = json.Unmarshal(asByte, &orgWallet)
		if err != nil {
			return shim.Error(err.Error())
		}

		if orgWallet.Token != updateAccountRequest.Wallet.Token {
			return shim.Error("Wrong account token!")
		}

		wallet := updateAccountRequest.Wallet
		wallet.DocType = "CoinWallet"
		wallet.Token = nextToken()

		asByte, err = json.Marshal(wallet)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(wallet.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}

		updateAccountResponse.Wallet = wallet
	}

	if (InvoiceStore{}) != updateAccountRequest.InvoiceStore {
		asByte, err = read(stub, updateAccountRequest.InvoiceStore.Id)
		if err != nil {
			return shim.Error(err.Error())
		}

		orgInvoiceStore := InvoiceStore{}
		err = json.Unmarshal(asByte, &orgInvoiceStore)
		if err != nil {
			return shim.Error(err.Error())
		}

		if orgInvoiceStore.Token != updateAccountRequest.InvoiceStore.Token {
			return shim.Error("Wrong invoiceStore token!")
		}

		invoiceStore := updateAccountRequest.InvoiceStore
		invoiceStore.DocType = "InvoiceStore"
		invoiceStore.Token = nextToken()

		asByte, err = json.Marshal(invoiceStore)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(invoiceStore.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}

		updateAccountResponse.InvoiceStore = invoiceStore
	}

	if (InvestmentApprovalBook{}) != updateAccountRequest.InvestmentApprovalBook {
		asByte, err = read(stub, updateAccountRequest.InvestmentApprovalBook.Id)
		if err != nil {
			return shim.Error(err.Error())
		}

		orgInvestmentApprovalBook := InvestmentApprovalBook{}
		err = json.Unmarshal(asByte, &orgInvestmentApprovalBook)
		if err != nil {
			return shim.Error(err.Error())
		}

		if orgInvestmentApprovalBook.Token != updateAccountRequest.InvestmentApprovalBook.Token {
			return shim.Error("Wrong investmentApprovalBook token!")
		}

		investmentApprovalBook := updateAccountRequest.InvestmentApprovalBook
		investmentApprovalBook.DocType = "InvestmentApprovalBook"
		investmentApprovalBook.Token = nextToken()

		asByte, err = json.Marshal(investmentApprovalBook)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(investmentApprovalBook.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}

		updateAccountResponse.InvestmentApprovalBook = investmentApprovalBook
	}

	asByte, err = json.Marshal(updateAccountResponse)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(asByte)
}

func (fc *FinBcContract) addProcessOrders(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	processOrdersRequest := ProcessOrdersRequest{}
	asByteRequest := []byte(args[0])
	err := json.Unmarshal(asByteRequest, &processOrdersRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	for _, processOrder := range processOrdersRequest.ProcessOrders {

		processOrder.DocType = "ProcessOrder"
		processOrder.Token = nextToken()

		asByte, err := json.Marshal(processOrder)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(processOrder.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}
	}

	return shim.Success(asByteRequest)
}

func (fc *FinBcContract) processInvestmentApprovalTokenTransfers(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	processInvestmentApprovalTokenTransfers := ProcessInvestmentApprovalTokenTransfersRequest{}
	asByteRequest := []byte(args[0])
	err := json.Unmarshal(asByteRequest, &processInvestmentApprovalTokenTransfers)
	if err != nil {
		return shim.Error(err.Error())
	}

	if processInvestmentApprovalTokenTransfers.InvestmentApprovalBook == (InvestmentApprovalBook{}) {
		return shim.Error("InvestmentApprovalBook isn't set!")
	}

	asByte, err := read(stub, processInvestmentApprovalTokenTransfers.InvestmentApprovalBook.Id)
	if err != nil {
		return shim.Error(err.Error())
	}

	orgInvestmentApprovalBook := InvestmentApprovalBook{}
	err = json.Unmarshal(asByte, &orgInvestmentApprovalBook)
	if err != nil {
		return shim.Error(err.Error())
	}

	if orgInvestmentApprovalBook.Token != processInvestmentApprovalTokenTransfers.InvestmentApprovalBook.Token {
		return shim.Error("Wrong investmentApprovalBook token!")
	}

	investmentApprovalBook := processInvestmentApprovalTokenTransfers.InvestmentApprovalBook
	investmentApprovalBook.DocType = "InvestmentApprovalBook"
	investmentApprovalBook.Token = nextToken()

	asByte, err = json.Marshal(investmentApprovalBook)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(investmentApprovalBook.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	for _, processOrderRef := range processInvestmentApprovalTokenTransfers.ProcessOrderRefs {

		asByte, err := read(stub, processOrderRef.Id)
		if err != nil {
			return shim.Error(err.Error())
		}

		processOrder := ProcessOrder{}
		err = json.Unmarshal(asByte, &processOrder)
		if err != nil {
			return shim.Error(err.Error())
		}

		if processOrder.Token != processOrderRef.Token {
			return shim.Error("Wrong processOrder token!")
		}

		if processOrder.State != "Open" {
			return shim.Error("ProcessOrder in wrong state!")
		}

		processOrder.State = "Done"
		processOrder.Token = nextToken()

		asByte, err = json.Marshal(processOrder)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(processOrder.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}
	}

	return shim.Success(asByteRequest)
}

func (fc *FinBcContract) processInvoicePayment(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	processInvoicePaymentRequest := ProcessInvoicePaymentRequest{}
	asByteRequest := []byte(args[0])
	err := json.Unmarshal(asByteRequest, &processInvoicePaymentRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	if processInvoicePaymentRequest.Invoice == (Invoice{}) {
		return shim.Error("Invoice isn't set!")
	}

	asByte, err := read(stub, processInvoicePaymentRequest.Invoice.Id)
	if err != nil {
		return shim.Error(err.Error())
	}

	orgInvoice := Invoice{}
	err = json.Unmarshal(asByte, &orgInvoice)
	if err != nil {
		return shim.Error(err.Error())
	}

	if orgInvoice.Token != processInvoicePaymentRequest.Invoice.Token {
		return shim.Error("Wrong invoice token!")
	}

	invoice := processInvoicePaymentRequest.Invoice
	invoice.DocType = "Invoice"
	invoice.Token = nextToken()

	asByte, err = json.Marshal(invoice)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(invoice.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	// update all wallets

	for _, wallet := range processInvoicePaymentRequest.Wallets {

		asByte, err := read(stub, wallet.Id)
		if err != nil {
			return shim.Error(err.Error())
		}

		orgWallet := CoinWallet{}
		err = json.Unmarshal(asByte, &orgWallet)
		if err != nil {
			return shim.Error(err.Error())
		}

		if orgWallet.Token != wallet.Token {
			return shim.Error("Wrong wallet token!")
		}

		wallet.DocType = "CoinWallet"
		wallet.Token = nextToken()

		asByte, err = json.Marshal(wallet)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(wallet.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}
	}

	// update processOrders

	for _, processOrderRef := range processInvoicePaymentRequest.ProcessOrderRefs {

		asByte, err := read(stub, processOrderRef.Id)
		if err != nil {
			return shim.Error(err.Error())
		}

		processOrder := ProcessOrder{}
		err = json.Unmarshal(asByte, &processOrder)
		if err != nil {
			return shim.Error(err.Error())
		}

		if processOrder.Token != processOrderRef.Token {
			return shim.Error("Wrong processOrder token!")
		}

		if processOrder.State != "Open" {
			return shim.Error("ProcessOrder in wrong state!")
		}

		processOrder.State = "Done"
		processOrder.Token = nextToken()

		asByte, err = json.Marshal(processOrder)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(processOrder.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}
	}

	return shim.Success(asByteRequest)

}

func (fc *FinBcContract) processInvoiceTokenTransfers(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	processInvoiceTokenTransfersRequest := ProcessInvoiceTokenTransfersRequest{}
	asByteRequest := []byte(args[0])
	err := json.Unmarshal(asByteRequest, &processInvoiceTokenTransfersRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	if processInvoiceTokenTransfersRequest.InvoiceStore == (InvoiceStore{}) {
		return shim.Error("InvoiceStore isn't set!")
	}

	asByte, err := read(stub, processInvoiceTokenTransfersRequest.InvoiceStore.Id)
	if err != nil {
		return shim.Error(err.Error())
	}

	orgInvoiceStore := InvoiceStore{}
	err = json.Unmarshal(asByte, &orgInvoiceStore)
	if err != nil {
		return shim.Error(err.Error())
	}

	if orgInvoiceStore.Token != processInvoiceTokenTransfersRequest.InvoiceStore.Token {
		return shim.Error("Wrong invoiceStore token!")
	}

	invoiceStore := processInvoiceTokenTransfersRequest.InvoiceStore
	invoiceStore.DocType = "InvoiceStore"
	invoiceStore.Token = nextToken()

	asByte, err = json.Marshal(invoiceStore)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(invoiceStore.Id, asByte)
	if err != nil {
		return shim.Error(err.Error())
	}

	// update processOrders

	for _, processOrderRef := range processInvoiceTokenTransfersRequest.ProcessOrderRefs {

		asByte, err := read(stub, processOrderRef.Id)
		if err != nil {
			return shim.Error(err.Error())
		}

		processOrder := ProcessOrder{}
		err = json.Unmarshal(asByte, &processOrder)
		if err != nil {
			return shim.Error(err.Error())
		}

		if processOrder.Token != processOrderRef.Token {
			return shim.Error("Wrong processOrder token!")
		}

		if processOrder.State != "Open" {
			return shim.Error("ProcessOrder in wrong state!")
		}

		processOrder.State = "Done"
		processOrder.Token = nextToken()

		asByte, err = json.Marshal(processOrder)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(processOrder.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}
	}

	return shim.Success(asByteRequest)

}

func (fc *FinBcContract) updateWallets(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	updateWalletsRequest := UpdateWalletsRequest{}
	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &updateWalletsRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	for _, coinWallet := range updateWalletsRequest.Wallets {

		// check signature with finbc pubkey

		// read wallet
		asByte, err = read(stub, coinWallet.Id)
		if err != nil {
			return shim.Error(err.Error())
		}

		coinWalletTo := CoinWallet{}
		err = json.Unmarshal(asByte, &coinWalletTo)
		if err != nil {
			return shim.Error(err.Error())
		}

		// check token
		if coinWalletTo.Token != coinWallet.Token {
			return shim.Error("Wrong token!")
		}

		coinWalletTo.EncOwnerValue = coinWallet.EncOwnerValue
		coinWalletTo.EncFinBcValue = coinWallet.EncFinBcValue
		coinWalletTo.Token = nextToken()

		asByte, err = json.Marshal(coinWalletTo)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(coinWalletTo.Id, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}

	}

	return shim.Success(asByte)
}

func (fc *FinBcContract) coinTransfer(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	coinWalletRequest := CoinWallet{}
	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &coinWalletRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = read(stub, coinWalletRequest.Owner)
	if err != nil {
		return shim.Error(err.Error())
	}

	coinWalletDB := CoinWallet{}
	err = json.Unmarshal(asByte, &coinWalletDB)

	token := make([]byte, 32)
	rand.Read(token)
	sum := sha256.Sum256(token)

	fmt.Printf("%x", sum)

	return shim.Success(asByte)
}

func (fc *FinBcContract) updateConfig(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	configRequest := ConfigRequest{}
	asByte := []byte(args[0])
	err := json.Unmarshal(asByte, &configRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	asByte, err = read(stub, CHAINCODE_CONFIG)
	if err != nil {

		configRequest.Config.DocType = "Config"
		configRequest.Config.Id = CHAINCODE_CONFIG
		configRequest.Config.Token = nextToken()

		asByte, err = json.Marshal(configRequest.Config)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(CHAINCODE_CONFIG, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}

		return shim.Success(asByte)

	} else {

		configOnLedger := Config{}
		err = json.Unmarshal(asByte, &configOnLedger)
		if err != nil {
			return shim.Error(err.Error())
		}

		if configOnLedger.Token != configRequest.Config.Token {
			return shim.Error("Wrong config token!")
		}

		//if(configOnLedger.FinBcAccount != "notsetyet") {
		// TODO check signature that only finbc account can update the address
		//}

		configRequest.Config.DocType = "Config"
		configRequest.Config.Id = CHAINCODE_CONFIG
		configRequest.Config.Token = nextToken()

		asByte, err = json.Marshal(configRequest.Config)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.PutState(CHAINCODE_CONFIG, asByte)
		if err != nil {
			return shim.Error(err.Error())
		}

		return shim.Success(asByte)

	}
}

func (fc *FinBcContract) query(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}
	var payload map[string]interface{}
	valAsBytes := []byte(args[0])
	uerr := json.Unmarshal(valAsBytes, &payload)
	if uerr != nil {
		return shim.Error(uerr.Error())
	}
	list := []interface{}{}
	result := make(map[string]interface{})
	ps := payload["query"]

	byteStr, err := json.Marshal(&ps)
	if err != nil {
		return shim.Error(uerr.Error())
	}
	qs := string(byteStr)

	fmt.Printf("Run Query: %s\n", qs)

	resultsIterator, err := stub.GetQueryResult(qs)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	fmt.Printf("Iterate over result\n")

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}

		fmt.Printf("Process result entry: %s\n", queryResponse.Value)

		var val map[string]interface{}
		xerr := json.Unmarshal(queryResponse.Value, &val)
		if xerr != nil {
			shim.Error(err.Error())
		}
		list = append(list, val)
	}

	fmt.Printf("Serialize result\n")

	result[payload["results"].(string)] = list
	resultAsByte, merr := json.Marshal(&result)
	if merr != nil {
		return shim.Error(merr.Error())
	}

	fmt.Printf("Return result: %s\n", string(resultAsByte))

	return shim.Success(resultAsByte)
}

func nextToken() string {

	data := make([]byte, 32)
	rand.Read(data)
	hash := sha256.New()
	hash.Write(data)

	return hex.EncodeToString(hash.Sum(nil))
}

func read(stub shim.ChaincodeStubInterface, key string) ([]byte, error) {
	valAsBytes, err := stub.GetState(key)
	if err != nil {
		return nil, err
	}
	return valAsBytes, nil
}

func main() {
	err := shim.Start(new(FinBcContract))
	if err != nil {
		fmt.Printf("Error creating new Contract: %s", err)
	}
}
